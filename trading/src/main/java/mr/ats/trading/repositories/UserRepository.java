package mr.ats.trading.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import mr.ats.trading.models.User;

public interface UserRepository extends JpaRepository<User, Long> {

}